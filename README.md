# pysignalclijsonrpc - Python API client for signal-cli JSON-RPC

Python client for [signal-cli 0.11.5+](https://github.com/AsamK/signal-cli/blob/master/CHANGELOG.md#0115---2022-11-07) native HTTP endpoint for JSON-RPC methods.

## Documentation

See https://pysignalclijsonrpc.readthedocs.io/

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
