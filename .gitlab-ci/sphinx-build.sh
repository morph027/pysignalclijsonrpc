#!/bin/sh

set -e

apk --no-cache --quiet --no-progress add \
    py3-virtualenv \
    build-base
python3 -m venv "${VENV}"
export PATH="${VENV}"/bin:"${PATH}"
pip3 install poetry
poetry install --with docs
sphinx-build -b html docs build
