Installation
============

Pypi
----

   .. code-block:: python

      pip install pysignalclijsonrpc


Development
-----------

   .. code-block:: python

      pip install git+https://gitlab.com/morph027/pysignalclijsonrpc#main
