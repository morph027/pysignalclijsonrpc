.. pysignalclijsonrpc documentation master file, created by
   sphinx-quickstart on Fri Sep 23 09:10:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pysignalclijsonrpc documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. toctree::
    :hidden:
    :caption: Usage

    installation/index
    usage/index
    autodoc/index


``pysignalclijsonrpc`` is a Python package providing access to `signal-cli JSON-RPC service <https://github.com/AsamK/signal-cli/wiki/JSON-RPC-service>`_.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`
