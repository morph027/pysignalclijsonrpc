SignalCliJSONRPCError
=====================

.. autoexception:: pysignalclijsonrpc.SignalCliJSONRPCError
   :members:
   :private-members:
